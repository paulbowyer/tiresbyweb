var loadContent = {
	items : [],
	itemNumber	:	undefined, 
	init	: 	function(src,startingPoint,endPoint){
		$(endPoint).load(src + ' ' + startingPoint,function(){
			$endPointContent = this;			
			loadContent.loadGrid();
			loadContent.displayGrid();
		});
	},
	loadGrid	: 	function(){
		$($endPointContent).children('.grid').children('tbody').children('tr').children('td').each(function(index,item){
			if(index > loadContent.itemNumber - 1){
				$(item).remove();
			}else{
				loadContent.items.push(item);
			}
		});
	},
	displayGrid	  :   function(){
		var ebayId = undefined;
		var imageSrc = undefined;
		var itemTitle = undefined;
		var itemLink = undefined;
		var itemPrice = undefined;
		loadContent.items.forEach(function(item,index){
			var firstTr = $(item).children('table').children('tbody').children('tr').first();
			var lastTr 	= $(item).children('table').children('tbody').children('tr').last();
			ebayId 		= firstTr.children('td').children('div').children('a').attr('id').replace('src','').toString();				
			imageSrc	= firstTr.children('td').children('div').children('a').children('img').attr('src')
			itemTitle 	= lastTr.children('td').children('div').first().children('a').attr('title');
			itemLink 	= lastTr.children('td').children('div').first().children('a').attr('href');
			itemPrice 	= lastTr.children('td').children('div').last().children('table').children('tbody').children('tr').first().children('td').last().text().replace('£','£ ');

		    // fill below...
			$('.featured_slide').eq(index + 0).children('div').children('img').attr('src', imageSrc);
			$('.featured_slide').eq(index + 0).children('.overlay').children('a').text(itemTitle);
			$('.featured_slide').eq(index + 0).children('.overlay').children('a').attr('href', itemLink);
			$('.featured_slide').eq(index + 0).children('.overlay').children('h3').text(itemPrice);
		});
	}
};

$(document).ready(function(){
	jQuery.support.cors = true;
	loadContent.itemNumber = 1;
	$('#CentralArea').after('<div class="bestMatches" style="display:none"></div>');
	loadContent.init('http://stores.ebay.co.uk/proofcamdashcams/_i.html?rt=nc&_sop=12&_sc=1', '.grid', '.bestMatches');
	var win_width = $(document).width();
	if (win_width >= 992) {
	    $('.featured_slider').attr('data-cycle-carousel-visible', '5');
	} else if (win_width >= 768) {
	    $('.featured_slider').attr('data-cycle-carousel-visible', '4');
	} else if (win_width >= 420) {
	    $('.featured_slider').attr('data-cycle-carousel-visible', '3');
	} else {
	    $('.featured_slider').attr('data-cycle-carousel-visible', '2');
	}
	setTimeout(function () {
	    $('.featured_slider').cycle();
	    $('.featured_slider').fadeIn(1000);
	}, 2000);
});







