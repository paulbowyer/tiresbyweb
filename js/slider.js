$(document).ready(function () {
    console.log = function () { }
   
    var win_width = $(document).width();
    if (win_width >= 992) {
        $('.featured_slider').attr('data-cycle-carousel-visible', '3');
    } else if (win_width >= 768) {
        $('.featured_slider').attr('data-cycle-carousel-visible', '3');
    } else if (win_width >= 420) {
        $('.featured_slider').attr('data-cycle-carousel-visible', '3');
    } else {
        $('.featured_slider').attr('data-cycle-carousel-visible', '2');
    }
    $('.featured_slider').cycle();

});
//console.log(win_width);